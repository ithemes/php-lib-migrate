<?php
declare(strict_types=1);

namespace iThemes\Lib\Migrate;

use iThemes\Lib\Migrate\Exception\InvalidArgumentException;
use iThemes\Lib\Migrate\Loader\Loader;
use iThemes\Lib\Migrate\Records\Repository;

final class Migrator
{
    private const D_UP = 'up';
    private const D_DOWN = 'down';

    /** @var Loader */
    private $loader;

    /** @var Repository */
    private $records;

    /** @var object */
    private $context;

    /**
     * Migrator constructor.
     *
     * @param Loader     $loader  Service to load Migration instances.
     * @param Repository $records Repository to store migration records.
     * @param object     $context Value passed to {@see Migration::up()} and {@see Migration::down}.
     *                            Typically a PDO instance or a query abstraction.
     */
    public function __construct(Loader $loader, Repository $records, object $context)
    {
        $this->loader  = $loader;
        $this->records = $records;
        $this->context = $context;
    }

    /**
     * Run available migrations.
     *
     * @param int|null $to Optionally, stop at the given version, running the given version.
     *
     * @return Result
     */
    public function up(?int $to = null): Result
    {
        $completed  = array_flip(listPluck($this->records->getCompleted(), 'getId'));
        $migrations = array_filter(
            $this->loader->load(),
            static function (Migration $migration) use ($completed, $to) {
                if (isset($completed[ $migration->getId() ])) {
                    return false;
                }

                if ($to !== null && $migration->getVersion() > $to) {
                    return false;
                }

                return true;
            }
        );

        return $this->runMigrations($migrations, self::D_UP);
    }

    /**
     * Run the down routine for migrations.
     *
     * @param int $to Stop at the given version, running the given version.
     *
     * @return Result
     */
    public function down(int $to): Result
    {
        $completed  = array_flip(listPluck($this->records->getCompleted(), 'getId'));
        $migrations = array_filter(
            $this->loader->load(),
            static function (Migration $migration) use ($completed, $to) {
                if (! isset($completed[ $migration->getId() ])) {
                    return false;
                }

                if ($migration->getVersion() < $to) {
                    return false;
                }

                return true;
            }
        );

        return $this->runMigrations($migrations, self::D_DOWN);
    }

    /**
     * Reset the db to its starting position by running all completed migrations in reverse.
     *
     * @param bool $hard Optionally, reset migrations that haven't been marked as completed as well.
     *
     * @return Result
     */
    public function reset(bool $hard = false): Result
    {
        $completed = array_flip(listPluck($this->records->getCompleted(), 'getId'));

        $migrations = $this->loader->load();

        if (! $hard) {
            $migrations = array_filter($migrations, static function (Migration $migration) use ($completed) {
                return isset($completed[ $migration->getId() ]);
            });
        }

        return $this->runMigrations($migrations, self::D_DOWN);
    }

    /**
     * Run the selected migrations.
     *
     * @param Migration[] $migrations
     * @param string      $direction Direction to run the migration. Either 'up' or 'down'.
     *
     * @return Result
     */
    private function runMigrations(array $migrations, string $direction): Result
    {
        if (! in_array($direction, [ self::D_UP, self::D_DOWN ], true)) {
            throw new InvalidArgumentException('Invalid migration direction.');
        }

        usort($migrations, static function (Migration $a, Migration $b) use ($direction) {
            switch ($direction) {
                case self::D_UP:
                    return $a->getVersion() <=> $b->getVersion();
                case self::D_DOWN:
                    return $b->getVersion() <=> $a->getVersion();
                default:
                    return 0;
            }
        });

        $result = new Result();
        $skip   = false;

        foreach ($migrations as $migration) {
            if ($skip) {
                $result->addSkipped($migration);
                continue;
            }

            try {
                switch ($direction) {
                    case self::D_UP:
                        $migration->up($this->context);
                        break;
                    case self::D_DOWN:
                        $migration->down($this->context);
                        break;
                }

                $result->addSuccess($migration);
            } catch (\Throwable $e) {
                $result->addError($migration, $e);
                $skip = true;
            }
        }

        $successes = array_filter($migrations, [ $result, 'isSuccess' ]);

        switch ($direction) {
            case self::D_UP:
                $this->records->record(...$successes);
                break;
            case self::D_DOWN:
                apply($successes, [ $this->records, 'delete' ]);
                break;
        }

        $result->seal();

        return $result;
    }
}
