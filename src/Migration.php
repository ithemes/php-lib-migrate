<?php
declare(strict_types=1);

namespace iThemes\Lib\Migrate;

interface Migration
{
    /**
     * Get the migration version.
     *
     * This will be a YYYYMMDDHHMMSS int.
     *
     * @return int
     */
    public function getVersion(): int;

    /**
     * Get the globally unique ID for the migration.
     *
     * @return string
     */
    public function getId(): string;

    /**
     * Run this migration.
     *
     * @param object $context Context passed to the Migrator during setup.
     */
    public function up(object $context): void;

    /**
     * Reverse this migration.
     *
     * @param object $context Context passed to the Migrator during setup.
     */
    public function down(object $context): void;

    /**
     * Returns the migration ID.
     *
     * @return string
     */
    public function __toString();
}
