<?php
declare(strict_types=1);

namespace iThemes\Lib\Migrate;

use iThemes\Lib\Migrate\Exception\MigrationNotAttempted;
use iThemes\Lib\Migrate\Exception\ResultSealed;

final class Result
{
    /** @var array */
    private $results = [];

    /** @var bool */
    private $sealed = false;

    /**
     * Mark that a migration was successful.
     *
     * @param Migration $migration
     *
     * @return Result
     */
    public function addSuccess(Migration $migration): self
    {
        $this->guardSealed();
        $this->results[ $migration->getId() ] = [
            'migration' => $migration,
            'success'   => true,
        ];

        return $this;
    }

    /**
     * Mark that a migration was unsuccessful.
     *
     * @param Migration  $migration
     * @param \Throwable $e
     *
     * @return Result
     */
    public function addError(Migration $migration, \Throwable $e): self
    {
        $this->guardSealed();
        $this->results[ $migration->getId() ] = [
            'migration' => $migration,
            'error'     => $e,
        ];

        return $this;
    }

    /**
     * Add a migration that was skipped because a previous migration errored out.
     *
     * @param Migration $migration
     *
     * @return Result
     */
    public function addSkipped(Migration $migration): self
    {
        $this->guardSealed();
        $this->results[ $migration->getId() ] = [
            'migration' => $migration,
            'skipped'   => true,
        ];

        return $this;
    }

    /**
     * Get all of the attempted migrations.
     *
     * @return Migration[]
     */
    public function getAttempted(): array
    {
        return array_values(listPluck($this->results, 'migration'));
    }

    /**
     * Check if the given migration was successfully completed.
     *
     * @param Migration $migration
     *
     * @return bool
     */
    public function isSuccess(Migration $migration): bool
    {
        $this->assertAttempted($migration);

        return ! empty($this->results[ $migration->getId() ]['success']);
    }

    /**
     * Was the given migration skipped.
     *
     * @param Migration $migration
     *
     * @return bool
     */
    public function isSkipped(Migration $migration): bool
    {
        $this->assertAttempted($migration);

        return ! empty($this->results[ $migration->getId() ]['skipped']);
    }

    /**
     * Did the given migration encounter an error.
     *
     * @param Migration $migration
     *
     * @return bool
     */
    public function isError(Migration $migration): bool
    {
        $this->assertAttempted($migration);

        return ! empty($this->results[ $migration->getId() ]['error']);
    }

    /**
     * If the migration was not a success, retrieve the error that occurred.
     *
     * This will throw a TypeError at runtime if the migration was successful.
     *
     * @param Migration $migration
     *
     * @return \Throwable
     */
    public function getError(Migration $migration): \Throwable
    {
        $this->assertAttempted($migration);

        return $this->results[ $migration->getId() ]['error'];
    }

    /**
     * Did any of the attempted migrations fail.
     *
     * @return bool
     */
    public function hasErrors(): bool
    {
        foreach ($this->getAttempted() as $migration) {
            if ($this->isError($migration)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Seal the Result from any new migration attempts being added.
     *
     * @return Result
     */
    public function seal(): self
    {
        $this->sealed = true;

        return $this;
    }

    private function assertAttempted(Migration $migration): void
    {
        if (! isset($this->results[ $migration->getId() ])) {
            throw new MigrationNotAttempted(
                $migration,
                sprintf("Migration '%s' was not attempted.", $migration->getId())
            );
        }
    }

    private function guardSealed(): void
    {
        if ($this->sealed) {
            throw new ResultSealed('Migration results are sealed.');
        }
    }
}
