<?php
declare(strict_types=1);

namespace iThemes\Lib\Migrate\CLI;

use iThemes\Lib\CLITools\Command\Command;
use function iThemes\Lib\Migrate\listPluck;
use iThemes\Lib\Migrate\Loader\Loader;
use iThemes\Lib\Migrate\Records\Repository;
use League\CLImate\CLImate;

class ListMigrations implements Command
{
    private const NAME = 'list';
    private const DESCRIPTION = 'List all registered migrations.';
    private const ARGUMENTS = [
        'format' => [
            'longPrefix'   => 'format',
            'defaultValue' => 'table',
            'description'  => 'Which format to output data. table, json, or ids.'
        ]
    ];

    /** @var Repository */
    private $repository;

    /** @var Loader */
    private $loader;

    /**
     * ListMigrations constructor.
     *
     * @param Repository $repository
     * @param Loader     $loader
     */
    public function __construct(Repository $repository, Loader $loader)
    {
        $this->repository = $repository;
        $this->loader     = $loader;
    }

    public function __invoke(CLImate $climate): int
    {
        $completed  = array_flip(listPluck($this->repository->getCompleted(), 'getId'));
        $migrations = $this->loader->load();

        $data = [];

        foreach ($migrations as $migration) {
            $data[] = [
                'id'        => $migration->getId(),
                'version'   => $migration->getVersion(),
                'completed' => isset($completed[ $migration->getId() ]),
            ];
        }

        switch ($climate->arguments->get('format')) {
            case 'table':
                $climate->table($data);
                break;
            case 'json':
                $climate->out(json_encode($data));
                break;
            case 'ids':
                $climate->out(implode(' ', listPluck($data, 'id')));
                break;
            default:
                $climate->error('Invalid output format.');

                return 1;
        }

        return 0;
    }

    public static function getName(): string
    {
        return self::NAME;
    }

    public static function getDescription(): string
    {
        return self::DESCRIPTION;
    }

    public static function getArguments(): array
    {
        return self::ARGUMENTS;
    }
}
