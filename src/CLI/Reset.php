<?php
declare(strict_types=1);

namespace iThemes\Lib\Migrate\CLI;

use iThemes\Lib\CLITools\Command\Command;
use iThemes\Lib\Migrate\Migrator;
use League\CLImate\CLImate;

class Reset implements Command
{
    private const NAME = 'reset';
    private const DESCRIPTION = 'Reset the database for the server.';
    private const ARGUMENTS = [
        'hard'   => [
            'noValue'     => true,
            'longPrefix'  => 'hard',
            'description' => 'Reset migrations that haven\'t been marked as completed as well.',
        ],
        'format' => [
            'longPrefix'   => 'format',
            'description'  => 'Which format to output data. summary, table, json.',
            'defaultValue' => 'summary',
        ],
    ];

    /** @var Migrator */
    private $migrator;

    /** @var ResultFormatter */
    private $formatter;

    /**
     * Reset constructor.
     *
     * @param Migrator        $migrator
     * @param ResultFormatter $formatter
     */
    public function __construct(Migrator $migrator, ResultFormatter $formatter)
    {
        $this->migrator  = $migrator;
        $this->formatter = $formatter;
    }

    public function __invoke(CLImate $climate): int
    {
        $format = $climate->arguments->get('format');

        if (! in_array($format, $this->formatter->getFormats(), true)) {
            $climate->to('error')->error('Invalid output format.');

            return 1;
        }

        $result = $this->migrator->reset($climate->arguments->get('hard'));

        $this->formatter->format($climate, $result, ResultFormatter::D_DOWN, $format);

        return $result->hasErrors() ? 1 : 0;
    }

    public static function getName(): string
    {
        return self::NAME;
    }

    public static function getDescription(): string
    {
        return self::DESCRIPTION;
    }

    public static function getArguments(): array
    {
        return self::ARGUMENTS;
    }
}
