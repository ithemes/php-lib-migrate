<?php
declare(strict_types=1);

namespace iThemes\Lib\Migrate\CLI;

use iThemes\Lib\Migrate\Migrator;
use League\CLImate\CLImate;
use iThemes\Lib\CLITools\Command\Command;
use iThemes\Lib\CLITools\Command\HasSubcommands;

class Migrate implements Command, HasSubcommands
{
    private const NAME = 'migrate';
    private const DESCRIPTION = 'Perform database migrations.';
    private const SUBCOMMANDS = [
        Down::class,
        Reset::class,
        ListMigrations::class,
        Generate::class,
    ];
    private const ARGUMENTS = [
        'to'     => [
            'longPrefix'  => 'to',
            'description' => 'Stop migrating at the given version (inclusive).',
            'castTo'      => 'int',
        ],
        'format' => [
            'longPrefix'   => 'format',
            'description'  => 'Which format to output data. summary, table, json.',
            'defaultValue' => 'summary',
        ],
    ];

    /** @var Migrator */
    private $migrator;

    /** @var ResultFormatter */
    private $formatter;

    /**
     * Migrate constructor.
     *
     * @param Migrator        $migrator
     * @param ResultFormatter $formatter
     */
    public function __construct(Migrator $migrator, ResultFormatter $formatter)
    {
        $this->migrator  = $migrator;
        $this->formatter = $formatter;
    }

    public function __invoke(CLImate $climate): int
    {
        $format = $climate->arguments->get('format');

        if (! in_array($format, $this->formatter->getFormats(), true)) {
            $climate->to('error')->error('Invalid output format.');

            return 1;
        }

        $to     = $climate->arguments->get('to') ?: null;
        $result = $this->migrator->up($to);

        $this->formatter->format($climate, $result, ResultFormatter::D_UP, $format);

        return $result->hasErrors() ? 1 : 0;
    }

    public static function getSubcommands(): array
    {
        return self::SUBCOMMANDS;
    }

    public static function getName(): string
    {
        return self::NAME;
    }

    public static function getDescription(): string
    {
        return self::DESCRIPTION;
    }

    public static function getArguments(): array
    {
        return self::ARGUMENTS;
    }
}
