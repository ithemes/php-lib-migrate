<?php
declare(strict_types=1);

namespace iThemes\Lib\Migrate\CLI;

use iThemes\Lib\Migrate\Generator\MigrationGenerator;
use iThemes\Lib\CLITools\Command\Command;
use League\CLImate\CLImate;
use Nette\PhpGenerator\PhpFile;

class Generate implements Command
{
    private const NAME = 'generate';
    private const DESCRIPTION = 'Generate a migration file.';
    private const ARGUMENTS = [
        'name' => [
            'description' => 'A name to identify the migration.',
            'required'    => true,
        ],
    ];

    /** @var MigrationGenerator */
    private $generator;

    /**
     * Generate constructor.
     *
     * @param MigrationGenerator $generator
     */
    public function __construct(MigrationGenerator $generator)
    {
        $this->generator = $generator;
    }

    public function __invoke(CLImate $climate): int
    {
        if (! class_exists(PhpFile::class)) {
            $climate->to('error')->error(
                'Dev Dependencies must be installed to generate migrations. Missing nette/php-generator.'
            );

            return 1;
        }

        $name = $climate->arguments->get('name');

        try {
            $filename = $this->generator->generate($name);
        } catch (\Throwable $e) {
            $climate->to('error')->error(sprintf('Failed to generate migration class: %s', $e->getMessage()));

            return 1;
        }

        $climate->info("Generated migration in {$filename}");

        return 0;
    }

    public static function getName(): string
    {
        return self::NAME;
    }

    public static function getDescription(): string
    {
        return self::DESCRIPTION;
    }

    public static function getArguments(): array
    {
        return self::ARGUMENTS;
    }
}
