<?php
declare(strict_types=1);

namespace iThemes\Lib\Migrate\CLI;

use iThemes\Lib\Migrate\Result;
use League\CLImate\CLImate;

final class ResultFormatter
{
    public const D_UP = 'up';
    public const D_DOWN = 'down';

    private const FORMATS = [
        'summary',
        'table',
        'json',
    ];

    /**
     * Format the migration results.
     *
     * @param CLImate $climate
     * @param Result  $result
     * @param string  $direction
     * @param string  $format
     */
    public function format(CLImate $climate, Result $result, string $direction, string $format): void
    {
        if ($format === 'summary') {
            $total   = count($result->getAttempted());
            $success = count(array_filter($result->getAttempted(), [ $result, 'isSuccess' ]));

            if ($direction === self::D_DOWN) {
                $data = sprintf('%d/%d migrations reversed.', $success, $total);
            } else {
                $data = sprintf('%d/%d migrations completed.', $success, $total);
            }

            if ($success === $total) {
                $climate->info($data);
            } else {
                $climate->to('error')->error($data);
            }

            foreach ($result->getAttempted() as $migration) {
                if ($result->isError($migration)) {
                    $climate->to('error')->error(sprintf(
                        '%s Error: %s',
                        $migration->getId(),
                        $result->getError($migration)->getMessage()
                    ));
                }
            }
        } else {
            $data = [];

            foreach ($result->getAttempted() as $migration) {
                $status = $error = '';

                if ($result->isSuccess($migration)) {
                    $status = 'success';
                } elseif ($result->isSkipped($migration)) {
                    $status = 'skipped';
                } elseif ($result->isError($migration)) {
                    $status = 'error';
                    $error  = $result->getError($migration)->getMessage();
                }

                $data[] = [
                    'id'     => $migration->getId(),
                    'status' => $status,
                    'error'  => $error,
                ];
            }

            switch ($format) {
                case 'table':
                    $climate->table($data);
                    break;
                case 'json':
                    $climate->out(json_encode($data));
                    break;
            }
        }
    }

    /**
     * Get the list of available formats.
     *
     * @return string[]
     */
    public function getFormats(): array
    {
        return self::FORMATS;
    }
}
