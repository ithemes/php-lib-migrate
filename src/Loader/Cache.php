<?php
declare(strict_types=1);

namespace iThemes\Lib\Migrate\Loader;

use iThemes\Lib\Migrate\Migration;

final class Cache implements Loader
{
    /** @var Loader */
    private $loader;

    /** @var Migration[] */
    private $cache;

    /**
     * Cache constructor.
     *
     * @param Loader $loader
     */
    public function __construct(Loader $loader)
    {
        $this->loader = $loader;
    }

    public function load(): array
    {
        if ($this->cache === null) {
            $this->cache = $this->loader->load();
        }

        return $this->cache;
    }
}
