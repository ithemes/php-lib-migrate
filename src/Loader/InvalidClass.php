<?php
declare(strict_types=1);

namespace iThemes\Lib\Migrate\Loader;

use iThemes\Lib\Migrate\Exception;

class InvalidClass extends \LogicException implements Exception
{

}
