<?php
declare(strict_types=1);

namespace iThemes\Lib\Migrate\Loader;

use iThemes\Lib\Migrate\Migration;

interface Loader
{
    /**
     * Load the collection of migrations.
     *
     * @return Migration[] List of migrations, keyed by id.
     */
    public function load(): array;
}
