<?php
declare(strict_types=1);

namespace iThemes\Lib\Migrate\Loader;

use iThemes\Lib\Migrate\Exception;

class ClassNotFound extends \LogicException implements Exception
{

}
