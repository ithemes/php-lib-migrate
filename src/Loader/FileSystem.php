<?php
declare(strict_types=1);

namespace iThemes\Lib\Migrate\Loader;

use iThemes\Lib\Migrate\Exception\InvalidArgumentException;
use iThemes\Lib\Migrate\Exception\IOException;
use iThemes\Lib\Migrate\Migration;
use Psr\Container\ContainerInterface;

final class FileSystem implements Loader
{
    /** @var string */
    private $migrationsDir;

    /** @var string */
    private $namespace;

    /** @var ContainerInterface */
    private $container;

    /**
     * FileSystem constructor.
     *
     * @param string $migrationsDir Directory to search in without trailing slash.
     * @param string $namespace     PHP Namespace without trailing backslash.
     */
    public function __construct(string $migrationsDir, string $namespace)
    {
        if (! is_dir($migrationsDir)) {
            throw new InvalidArgumentException('Migrations directory does not exist.');
        }

        $this->migrationsDir = $migrationsDir;
        $this->namespace     = $namespace;
    }

    /**
     * Set the container to use for loading migration definitions.
     *
     * @param ContainerInterface $container
     */
    public function setContainer(ContainerInterface $container): void
    {
        $this->container = $container;
    }

    public function load(): array
    {
        $files = glob("{$this->migrationsDir}/*.php", GLOB_NOSORT);

        if (false === $files) {
            throw new IOException('Failed to glob migrations directory.');
        }

        $migrations = [];

        foreach ($files as $file) {
            require_once $file;

            $filename = pathinfo($file, PATHINFO_FILENAME);
            $class    = $this->namespace . '\\' . $filename;

            if (! class_exists($class)) {
                throw new ClassNotFound(
                    sprintf("Class '%s' not found for '%s' migration file.", $class, basename($filename))
                );
            }

            if (! is_subclass_of($class, Migration::class)) {
                throw new InvalidClass(
                    sprintf("Migration '%s' does not implement Migration interface.", $class)
                );
            }

            /** @var Migration $migration */
            if ($this->container && $this->container->has($class)) {
                $migration = $this->container->get($class);
            } else {
                $migration = new $class();
            }

            $migrations[ $migration->getId() ] = $migration;
        }

        return $migrations;
    }
}
