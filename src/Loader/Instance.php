<?php
declare(strict_types=1);

namespace iThemes\Lib\Migrate\Loader;

use function iThemes\Lib\Migrate\keyBy;
use iThemes\Lib\Migrate\Migration;

final class Instance implements Loader
{
    /** @var Migration[] */
    private $migrations;

    /**
     * Instance constructor.
     *
     * @param Migration ...$migrations
     */
    public function __construct(Migration ...$migrations)
    {
        $this->migrations = keyBy($migrations, 'getId');
    }

    public function load(): array
    {
        return $this->migrations;
    }
}
