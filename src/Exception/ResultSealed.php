<?php
declare(strict_types=1);

namespace iThemes\Lib\Migrate\Exception;

use iThemes\Lib\Migrate\Exception;

class ResultSealed extends \LogicException implements Exception
{

}
