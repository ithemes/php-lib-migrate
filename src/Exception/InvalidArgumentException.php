<?php
declare(strict_types=1);

namespace iThemes\Lib\Migrate\Exception;

use iThemes\Lib\Migrate\Exception;

class InvalidArgumentException extends \InvalidArgumentException implements Exception
{

}
