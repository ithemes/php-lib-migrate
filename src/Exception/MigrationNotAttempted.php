<?php
declare(strict_types=1);

namespace iThemes\Lib\Migrate\Exception;

use iThemes\Lib\Migrate\Exception;
use iThemes\Lib\Migrate\Migration;
use Throwable;

class MigrationNotAttempted extends \LogicException implements Exception
{
    /** @var Migration */
    private $migration;

    public function __construct(Migration $migration, $message = '', $code = 0, Throwable $previous = null)
    {
        $this->migration = $migration;
        parent::__construct($message, $code, $previous);
    }

    /**
     * Get the migration that was not attempted.
     *
     * @return Migration
     */
    public function getMigration(): Migration
    {
        return $this->migration;
    }
}
