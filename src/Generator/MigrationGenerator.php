<?php
declare(strict_types=1);

namespace iThemes\Lib\Migrate\Generator;

use iThemes\Lib\Migrate\Exception\IOException;

interface MigrationGenerator
{
    /**
     * Generate the Class and File for the given migration name.
     *
     * @param string $name
     *
     * @return string
     *
     * @throws IOException
     */
    public function generate(string $name): string;
}
