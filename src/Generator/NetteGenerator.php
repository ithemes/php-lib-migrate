<?php
declare(strict_types=1);

namespace iThemes\Lib\Migrate\Generator;

use iThemes\Lib\Migrate\Exception\IOException;
use Nette\PhpGenerator\ClassType;
use Nette\PhpGenerator\PhpFile;
use Nette\PhpGenerator\PhpLiteral;
use Nette\PhpGenerator\PsrPrinter;

trait NetteGenerator
{
    private function createPhpFile(): PhpFile
    {
        $file = new PhpFile();
        $file->setStrictTypes();

        return $file;
    }

    private function implementGetVersion(ClassType $class, int $version): void
    {
        $class
            ->addMethod('getVersion')
            ->setVisibility(ClassType::VISIBILITY_PUBLIC)
            ->setReturnType('int')
            ->setBody('return ?;', [ $version ]);
    }

    private function implementGetId(ClassType $class): void
    {
        $class
            ->addMethod('getId')
            ->setVisibility(ClassType::VISIBILITY_PUBLIC)
            ->setReturnType('string')
            ->setBody('return ?;', [ $class->getName() ]);
    }

    private function implementUp(ClassType $class): void
    {
        $class
            ->addMethod('up')
            ->setVisibility(ClassType::VISIBILITY_PUBLIC)
            ->setReturnType('void')
            ->addParameter('context')
            ->setTypeHint('object');
    }

    private function implementDown(ClassType $class): void
    {
        $class
            ->addMethod('down')
            ->setVisibility(ClassType::VISIBILITY_PUBLIC)
            ->setReturnType('void')
            ->addParameter('context')
            ->setTypeHint('object');
    }

    private function implementToString(ClassType $class): void
    {
        $class
            ->addMethod('__toString')
            ->setVisibility(ClassType::VISIBILITY_PUBLIC)
            ->setBody('return ?;', [ new PhpLiteral('$this->getId()') ]);
    }

    /**
     * Write the PhpFile out to the given filename.
     *
     * @param PhpFile $file
     * @param string  $filename
     */
    private function out(PhpFile $file, string $filename): void
    {
        $fh = fopen($filename, 'wb+');

        if (! $fh) {
            throw new IOException('Could not open file.');
        }

        if (! fwrite($fh, (new PsrPrinter())->printFile($file))) {
            throw new IOException('Could not write to file.');
        }

        fclose($fh);
    }
}
