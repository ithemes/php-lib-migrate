<?php
declare(strict_types=1);

namespace iThemes\Lib\Migrate;

/**
 * Pluck fields from a list.
 *
 * @param iterable $iterable
 * @param string   $field
 *
 * @return array
 */
function listPluck(iterable $iterable, string $field): array
{
    $arr = [];

    foreach ($iterable as $key => $value) {
        $arr[ $key ] = get($value, $field);
    }

    return $arr;
}

/**
 * Key a list by each list member's value of the given field.
 *
 * @param iterable $iterable
 * @param string   $field
 *
 * @return array
 */
function keyBy(iterable $iterable, string $field): array
{
    $keyed = [];

    foreach ($iterable as $value) {
        $key = get($value, $field);

        $keyed[ $key ] = $value;
    }

    return $keyed;
}

/**
 * Get a value from the given object.
 *
 * @param array|object $value
 * @param string       $field An array key, object member, or object method.
 * @param mixed        $default
 *
 * @return mixed|null
 */
function get($value, string $field, $default = null)
{
    if (is_array($value)) {
        return $value[ $field ] ?? $default;
    }

    if (is_object($value)) {
        if (is_callable([ $value, $field ])) {
            return $value->{$field}();
        }

        if ($value instanceof \ArrayAccess) {
            return $value[ $field ] ?? $default;
        }

        return $value->$field ?? $default;
    }

    return $default;
}

/**
 * Call a function on every element of a list.
 *
 * @param iterable $iterable
 * @param callable $callable
 */
function apply(iterable $iterable, callable $callable): void
{
    foreach ($iterable as $key => $value) {
        $callable($value, $key);
    }
}
