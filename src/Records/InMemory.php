<?php
declare(strict_types=1);

namespace iThemes\Lib\Migrate\Records;

use iThemes\Lib\Migrate\Migration;

final class InMemory implements Repository
{
    /** @var Record[] */
    private $completed = [];

    /** @var int */
    private $currentBatch = 0;

    public function getCompleted(): array
    {
        return $this->completed;
    }

    public function record(Migration ...$migrations): array
    {
        $records = [];
        $this->currentBatch++;

        foreach ($migrations as $migration) {
            $records[] = new Record($migration->getId(), $this->currentBatch);
        }

        $this->completed = array_merge($this->completed, $records);

        return $records;
    }

    public function delete(Migration $migration): void
    {
        foreach ($this->completed as $i => $record) {
            if ($record->getId() === $migration->getId()) {
                unset($this->completed[ $i ]);
            }
        }
    }
}
