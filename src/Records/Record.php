<?php
declare(strict_types=1);

namespace iThemes\Lib\Migrate\Records;

final class Record
{
    /** @var string */
    private $id;

    /** @var int */
    private $batch;

    /**
     * Record constructor.
     *
     * @param string $id
     * @param int    $batch
     */
    public function __construct(string $id, int $batch)
    {
        $this->id    = $id;
        $this->batch = $batch;
    }

    /**
     * Get the completed migration's ID.
     *
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * Get the batch number the migration was completed in.
     *
     * @return int
     */
    public function getBatch(): int
    {
        return $this->batch;
    }
}
