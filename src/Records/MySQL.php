<?php
declare(strict_types=1);

namespace iThemes\Lib\Migrate\Records;

use iThemes\Lib\Migrate\Migration;

final class MySQL implements Repository
{
    /** @var \PDO */
    private $pdo;

    /** @var string */
    private $tableName;

    /**
     * MySQL constructor.
     *
     * @param \PDO   $pdo
     * @param string $tableName
     */
    public function __construct(\PDO $pdo, string $tableName = 'migrations')
    {
        $this->pdo       = $pdo;
        $this->tableName = $tableName;
    }

    public function getCompleted(): array
    {
        $completed = $this->pdo->query("SELECT `migration`, `batch` FROM `{$this->tableName}`")->fetchAll();
        $records   = [];

        foreach ($completed as $record) {
            $records[] = new Record($record['migration'], (int) $record['batch']);
        }

        return $records;
    }

    public function record(Migration ...$migrations): array
    {
        if (! $migrations) {
            return [];
        }

        $batch = $this->getNextBatchNumber();

        $sql     = "INSERT INTO `{$this->tableName}` (`migration`, `batch`) VALUES ";
        $prepare = [];
        $records = [];

        foreach ($migrations as $migration) {
            $sql .= '(?, ?), ';

            $prepare[] = $migration->getId();
            $prepare[] = $batch;

            $records[] = new Record($migration->getId(), $batch);
        }

        $sql = substr($sql, 0, -2);

        $this->pdo->prepare($sql)->execute($prepare);

        return $records;
    }

    public function delete(Migration $migration): void
    {
        $this->pdo->prepare("DELETE FROM `{$this->tableName}` WHERE `migration` = ?")->execute([ $migration->getId() ]);
    }

    /**
     * Install the migrations table.
     */
    public function install(): void
    {
        $sql = "CREATE TABLE `{$this->tableName}` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci";

        $this->pdo->exec($sql);
    }

    /**
     * Get the next batch number to use.
     *
     * @return int
     */
    private function getNextBatchNumber(): int
    {
        return $this->getLastBatchNumber() + 1;
    }

    /**
     * Get the latest batch number.
     *
     * @return int
     */
    private function getLastBatchNumber(): int
    {
        $row = $this->pdo->query("SELECT max(`batch`) as `last_batch` FROM `{$this->tableName}`")->fetch();

        return (int) $row['last_batch'];
    }
}
