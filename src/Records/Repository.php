<?php
declare(strict_types=1);

namespace iThemes\Lib\Migrate\Records;

use iThemes\Lib\Migrate\Migration;

interface Repository
{
    /**
     * Get the list of completed migrations.
     *
     * @return Record[]
     */
    public function getCompleted(): array;

    /**
     * Record a list of migrations.
     *
     * @param Migration ...$migrations
     *
     * @return Record[]
     */
    public function record(Migration ...$migrations): array;

    /**
     * Delete a migration record.
     *
     * @param Migration $migration
     */
    public function delete(Migration $migration): void;
}
