<?php
declare(strict_types=1);

namespace iThemes\Lib\Migrate\Integrations\Doctrine;

use Doctrine\DBAL\Schema\Schema;
use iThemes\Lib\Migrate\Generator\MigrationGenerator;
use iThemes\Lib\Migrate\Generator\NetteGenerator;
use Nette\PhpGenerator\ClassType;

final class DoctrineGenerator implements MigrationGenerator
{
    use NetteGenerator;

    private const DATE_FORMAT = 'YmdHis';

    /** @var string */
    private $migrationsDir;

    /** @var string */
    private $namespace;

    /**
     * MigrationGenerator constructor.
     *
     * @param string $migrationsDir Absolute path to directory containing migrations without trailing slash.
     * @param string $namespace     Class Namespace without trailing backslash.
     */
    public function __construct(string $migrationsDir, string $namespace)
    {
        $this->migrationsDir = $migrationsDir;
        $this->namespace     = $namespace;
    }

    public function generate(string $name): string
    {
        $version = (int) gmdate(self::DATE_FORMAT);

        $classname = "{$name}{$version}";
        $filename  = "{$classname}.php";

        $file = $this->createPhpFile();

        $namespace = $file->addNamespace(trim($this->namespace, '\\'));
        $class     = $namespace->addClass($classname);
        $class->addExtend(DoctrineMigration::class);

        $this->implementGetVersion($class, $version);
        $this->implementGetId($class);

        $class
            ->addMethod('configureUp')
            ->setVisibility(ClassType::VISIBILITY_PROTECTED)
            ->setReturnType('void')
            ->addParameter('schema')
            ->setTypeHint(Schema::class);

        $class
            ->addMethod('configureDown')
            ->setVisibility(ClassType::VISIBILITY_PROTECTED)
            ->setReturnType('void')
            ->addParameter('schema')
            ->setTypeHint(Schema::class);

        $this->implementToString($class);

        $this->out($file, $this->migrationsDir . '/' . $filename);

        return $filename;
    }
}
