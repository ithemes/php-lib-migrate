<?php
declare(strict_types=1);

namespace iThemes\Lib\Migrate\Integrations\Doctrine;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Schema\Schema;
use iThemes\Lib\Migrate\Migration;

abstract class DoctrineMigration implements Migration
{
    /**
     * Configure the schema to the new desired state.
     *
     * @param Schema $schema
     */
    abstract protected function configureUp(Schema $schema): void;

    /**
     * Reverse the migration.
     *
     * @param Schema $schema
     */
    abstract protected function configureDown(Schema $schema): void;

    public function up(object $context): void
    {
        assert($context instanceof Connection);

        $fromSchema = $context->getSchemaManager()->createSchema();

        $toSchema = clone $fromSchema;
        $this->configureUp($toSchema);

        $sql = $fromSchema->getMigrateToSql($toSchema, $context->getDatabasePlatform());

        $context->transactional(static function () use ($sql, $context) {
            array_walk($sql, [ $context, 'exec' ]);
        });
    }

    public function down(object $context): void
    {
        assert($context instanceof Connection);

        $fromSchema = $context->getSchemaManager()->createSchema();

        $toSchema = clone $fromSchema;
        $this->configureDown($toSchema);

        $sql = $fromSchema->getMigrateToSql($toSchema, $context->getDatabasePlatform());

        $context->transactional(static function () use ($sql, $context) {
            array_walk($sql, [ $context, 'exec' ]);
        });
    }
}
