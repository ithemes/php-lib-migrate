<?php
declare(strict_types=1);

namespace iThemes\Lib\Migrate\Integrations;

use iThemes\Lib\Migrate\CLI;
use iThemes\Lib\Migrate\Exception\InvalidArgumentException;
use iThemes\Lib\Migrate\Generator\MigrationGenerator;
use iThemes\Lib\Migrate\Loader\Loader;
use iThemes\Lib\Migrate\Generator\SimpleMigrationGenerator;
use iThemes\Lib\Migrate\Migrator;
use iThemes\Lib\Migrate\Records\Repository;

class PimpleServiceProvider implements \Pimple\ServiceProviderInterface
{
    /** @var callable */
    private $contextFactory;

    /**
     * PimpleServiceProvider constructor.
     *
     * @param callable $contextFactory
     */
    public function __construct(callable $contextFactory)
    {
        $this->contextFactory = $contextFactory;
    }

    /**
     * Register migration services on the Container.
     *
     * It is required to register a Loader and Repository definition on the container.
     *
     * @param \Pimple\Container $container
     */
    public function register(\Pimple\Container $container)
    {
        $container[ Migrator::class ] = function ($container) {
            $factory = $this->contextFactory;

            return new Migrator($container[ Loader::class ], $container[ Repository::class ], $factory());
        };

        // ------ CLI ------ //

        $container[ CLI\ResultFormatter::class ] = static function () {
            return new CLI\ResultFormatter();
        };

        $container[ CLI\Migrate::class ] = static function ($container) {
            return new CLI\Migrate($container[ Migrator::class ], $container[ CLI\ResultFormatter::class ]);
        };

        $container[ CLI\Down::class ] = static function ($container) {
            return new CLI\Down($container[ Migrator::class ], $container[ CLI\ResultFormatter::class ]);
        };

        $container[ CLI\Reset::class ] = static function ($container) {
            return new CLI\Reset($container[ Migrator::class ], $container[ CLI\ResultFormatter::class ]);
        };

        $container[ CLI\ListMigrations::class ] = static function ($container) {
            return new CLI\ListMigrations($container[ Repository::class ], $container[ Loader::class ]);
        };

        if (isset($container[ MigrationGenerator::class ])) {
            $container[ CLI\Generate::class ] = static function ($container) {
                return new CLI\Generate($container[ MigrationGenerator::class ]);
            };
        }
    }
}
