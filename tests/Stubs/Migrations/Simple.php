<?php
declare(strict_types=1);

namespace iThemes\Lib\Migrate\Tests\Stubs\Migrations;

use iThemes\Lib\Migrate\Migration;
use PDO;

class Simple implements Migration
{
    /** @var int */
    private $version;

    /** @var string */
    private $id;

    /**
     * Simple constructor.
     *
     * @param int    $version
     * @param string $id
     */
    public function __construct(int $version = 20190611, string $id = 'simple_20190611')
    {
        $this->version = $version;
        $this->id      = $id;
    }

    public function getVersion(): int
    {
        return $this->version;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function up(object $context): void
    {
    }

    public function down(object $context): void
    {
    }

    public function __toString()
    {
        return $this->getId();
    }
}
