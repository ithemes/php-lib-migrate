<?php
declare(strict_types=1);

namespace iThemes\Lib\Migrate\Tests\Stubs\Doctrine;

use Doctrine\DBAL\Schema\Schema;
use iThemes\Lib\Migrate\Integrations\Doctrine\DoctrineMigration;

class ConfigurableDoctrineMigration extends DoctrineMigration
{
    /** @var int */
    private $version;

    /** @var string */
    private $id;
    /**
     * @var callable
     */
    private $up;
    /**
     * @var callable
     */
    private $down;

    /**
     * Simple constructor.
     *
     * @param callable $up
     * @param callable $down
     * @param int      $version
     * @param string   $id
     */
    public function __construct(callable $up, callable $down, int $version = 20190611, string $id = 'dbal_20190611')
    {
        $this->up      = $up;
        $this->down    = $down;
        $this->version = $version;
        $this->id      = $id;
    }

    protected function configureUp(Schema $schema): void
    {
        call_user_func($this->up, $schema);
    }

    protected function configureDown(Schema $schema): void
    {
        call_user_func($this->down, $schema);
    }

    public function getVersion(): int
    {
        return $this->version;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function __toString()
    {
        return $this->getId();
    }
}
