<?php

namespace iThemes\Lib\Migrate\Tests\Stubs\MigrationsClassMismatch;

use iThemes\Lib\Migrate\Migration;
use PDO;

class MismatchedName implements Migration
{
    public function getVersion(): int
    {
        return 1;
    }

    public function getId(): string
    {
        return 'test_id';
    }

    public function up(object $context): void
    {
    }

    public function down(object $context): void
    {
    }

    public function __toString()
    {
        return $this->getId();
    }
}
