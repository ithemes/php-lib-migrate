<?php
declare(strict_types=1);

namespace iThemes\Lib\Migrate\Tests\Suites\Integration\Records;

use iThemes\Lib\Migrate\Records\MySQL;
use iThemes\Lib\Migrate\Records\Repository;

class MySQLTest extends RepositoryTest
{
    /** @var MySQL */
    private static $repo;

    /** @var \PDO */
    private static $pdo;

    /** @var string */
    private static $tn;

    public static function setUpBeforeClass(): void
    {
        parent::setUpBeforeClass();

        if (! getenv('MYSQL_DB')) {
            return;
        }

        static::$tn = $tn = 'migrations_' . substr(uniqid('', false), 0, 5);

        static::$pdo = new \PDO(
            sprintf('mysql:dbname=%s;host=%s;', getenv('MYSQL_DB'), getenv('MYSQL_HOST')),
            getenv('MYSQL_USER') ?: '',
            getenv('MYSQL_PW') ?: ''
        );
        static::$pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);

        static::$repo = new MySQL(static::$pdo, $tn);

        if (static::$pdo->query("SHOW TABLES LIKE '{$tn}'")->rowCount()) {
            static::$pdo->exec("DROP TABLE `{$tn}`");
        }

        static::$repo->install();
    }

    protected function setUp(): void
    {
        if (! static::$pdo) {
            self::markTestSkipped('MySQL DSN not available.');
        }

        parent::setUp();
    }

    protected function tearDown(): void
    {
        parent::tearDown();

        static::$pdo->exec(sprintf('TRUNCATE `%s`', static::$tn));
    }

    public static function tearDownAfterClass(): void
    {
        parent::tearDownAfterClass();

        if (! getenv('MYSQL_DB')) {
            return;
        }

        static::$pdo->exec(sprintf('DROP TABLE `%s`', static::$tn));
        static::$repo = null;
        static::$pdo  = null;
    }

    protected function getRepository(): Repository
    {
        return static::$repo;
    }
}
