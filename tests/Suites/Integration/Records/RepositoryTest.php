<?php
declare(strict_types=1);

namespace iThemes\Lib\Migrate\Tests\Suites\Integration\Records;

use function iThemes\Lib\Migrate\listPluck;
use iThemes\Lib\Migrate\Records\Repository;
use iThemes\Lib\Migrate\Tests\Stubs\Migrations\Simple;
use PHPUnit\Framework\TestCase;

abstract class RepositoryTest extends TestCase
{
    /**
     * Get the repository to test.
     *
     * Will be called once per test case.
     *
     * Should not maintain state between calls.
     *
     * @return Repository
     */
    abstract protected function getRepository(): Repository;

    public function testReturnsEmptyArrayWithNoRecords(): void
    {
        self::assertCount(0, $this->getRepository()->getCompleted());
    }

    public function testRecordNonePassedDoesNotError(): void
    {
        $records = $this->getRepository()->record();
        self::assertCount(0, $records);
    }

    public function testRecord(): void
    {
        $repo = $this->getRepository();

        $migration = new Simple();
        $records   = $repo->record($migration);

        self::assertCount(1, $records);
        self::assertEquals($migration->getId(), $records[0]->getId());

        $completed = $repo->getCompleted();
        self::assertCount(1, $completed);
        self::assertEquals($migration->getId(), $completed[0]->getId());
    }

    public function testRecordMultipleHaveSameBatch(): void
    {
        $repo = $this->getRepository();

        $m1 = new Simple();
        $m2 = new Simple(20190612, 'simple_20190612');

        $records = $repo->record($m1, $m2);

        self::assertCount(2, $records);
        self::assertEquals($m1->getId(), $records[0]->getId());
        self::assertEquals($m2->getId(), $records[1]->getId());

        $batch = $records[0]->getBatch();
        self::assertEquals($batch, $records[1]->getBatch());

        // --- Completed --- //
        $completed = $repo->getCompleted();

        self::assertCount(2, $completed);
        self::assertEquals($m1->getId(), $completed[0]->getId());
        self::assertEquals($m2->getId(), $completed[1]->getId());

        self::assertEquals($batch, $completed[0]->getBatch());
        self::assertEquals($batch, $completed[1]->getBatch());
    }

    public function testRecordDuplicateMigrationsKeepsBothRecords(): void
    {
        $repo      = $this->getRepository();
        $migration = new Simple();

        $repo->record($migration, $migration);
        $repo->record($migration);

        $completed = $repo->getCompleted();
        self::assertCount(3, $completed);

        foreach ($completed as $record) {
            self::assertEquals($migration->getId(), $record->getId());
        }

        self::assertEquals($completed[0]->getBatch(), $completed[1]->getBatch());
        self::assertNotEquals($completed[0]->getBatch(), $completed[2]->getBatch());
    }

    public function testDelete(): void
    {
        $repo      = $this->getRepository();
        $migration = new Simple();

        $repo->record($migration);
        self::assertCount(1, $repo->getCompleted());

        $repo->delete($migration);
        self::assertCount(0, $repo->getCompleted());
    }

    public function testDeleteOnlyDeletesCorrectMigration(): void
    {
        $repo = $this->getRepository();

        $m1 = new Simple();
        $m2 = new Simple(20190612, 'simple_20190612');
        $m3 = new Simple(20190613, 'simple_20190613');

        $repo->record($m1, $m2);
        $repo->record($m3);

        self::assertCount(3, $repo->getCompleted());

        $repo->delete($m2);

        $completed = listPluck($repo->getCompleted(), 'getId');
        self::assertCount(2, $completed);
        self::assertNotContains($m2->getId(), $completed);
    }

    public function testDeleteDoesNotErrorIfMigrationDoesNotExist(): void
    {
        $this->getRepository()->delete(new Simple());
        $this->addToAssertionCount(1);
    }
}
