<?php
declare(strict_types=1);

namespace iThemes\Lib\Migrate\Tests\Suites\Integration\Records;

use iThemes\Lib\Migrate\Records\InMemory;
use iThemes\Lib\Migrate\Records\Repository;

class InMemoryTest extends RepositoryTest
{
    protected function getRepository(): Repository
    {
        return new InMemory();
    }
}
