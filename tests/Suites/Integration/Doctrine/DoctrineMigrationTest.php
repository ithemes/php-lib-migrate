<?php
declare(strict_types=1);

namespace iThemes\Lib\Migrate\Tests\Suites\Integration\Doctrine;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\DBALException;
use Doctrine\DBAL\DriverManager;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\DBAL\Types\Type;
use iThemes\Lib\Migrate\Tests\Stubs\Doctrine\ConfigurableDoctrineMigration;
use PHPUnit\Framework\TestCase;

class DoctrineMigrationTest extends TestCase
{
    /** @var Connection */
    private static $conn;

    /** @var string */
    private static $tn;

    public static function setUpBeforeClass(): void
    {
        parent::setUpBeforeClass();

        if (! getenv('MYSQL_DB')) {
            self::markTestSkipped('MySQL DSN not available.');
        }

        static::$tn = $tn = 'test' . substr(uniqid('', false), 0, 5);

        static::$conn = DriverManager::getConnection([
            'driver'   => 'pdo_mysql',
            'host'     => getenv('MYSQL_HOST'),
            'dbname'   => getenv('MYSQL_DB'),
            'user'     => getenv('MYSQL_USER') ?: '',
            'password' => getenv('MYSQL_PW') ?: '',
        ]);

        if (static::$conn->query("SHOW TABLES LIKE '{$tn}'")->rowCount()) {
            static::$conn->exec("DROP TABLE `{$tn}`");
        }
    }

    public static function tearDownAfterClass(): void
    {
        parent::tearDownAfterClass();

        if (! getenv('MYSQL_DB')) {
            return;
        }

        static::$conn->exec(sprintf('DROP TABLE IF EXISTS `%s`', static::$tn));
        static::$conn = null;
    }

    public function testUpAndDown(): void
    {
        $tn = static::$tn;

        $migration = new ConfigurableDoctrineMigration(
            static function (Schema $schema) {
                $schema->createTable(static::$tn)
                    ->addColumn('testCol', Type::TEXT);
            },
            static function (Schema $schema) {
                $schema->dropTable(static::$tn);
            }
        );

        $migration->up(static::$conn);
        self::assertEquals(1, static::$conn->query("SHOW TABLES LIKE '{$tn}'")->rowCount());

        $migration->down(static::$conn);
        self::assertEquals(0, static::$conn->query("SHOW TABLES LIKE '{$tn}'")->rowCount());
    }

    public function testRollbackIncompleteMigration(): void
    {
        $tn = static::$tn;

        $migration = new ConfigurableDoctrineMigration(
            static function (Schema $schema) {
                $table = $schema->createTable(static::$tn);
                $table->addColumn('testCol', Type::TEXT);
                $table->addColumn('invalid-col', Type::TEXT);
            },
            static function (Schema $schema) {
                $schema->dropTable(static::$tn);
            }
        );

        $this->expectException(DBALException::class);
        $migration->up(static::$conn);
        self::assertEquals(0, static::$conn->query("SHOW TABLES LIKE '{$tn}'")->rowCount());
    }
}
