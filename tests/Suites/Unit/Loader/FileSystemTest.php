<?php
declare(strict_types=1);

namespace iThemes\Lib\Migrate\Tests\Suites\Unit\Loader;

use iThemes\Lib\Migrate\Exception\InvalidArgumentException;
use iThemes\Lib\Migrate\Loader\ClassNotFound;
use iThemes\Lib\Migrate\Loader\FileSystem;
use iThemes\Lib\Migrate\Loader\InvalidClass;
use iThemes\Lib\Migrate\Tests\Stubs\Migrations\Simple;
use PHPUnit\Framework\TestCase;
use Psr\Container\ContainerInterface;

class FileSystemTest extends TestCase
{
    public function testLoad(): void
    {
        $loader = new FileSystem(
            realpath(__DIR__ . '/../../../Stubs/Migrations'),
            'iThemes\\Lib\\Migrate\\Tests\\Stubs\\Migrations'
        );

        $loaded = $loader->load();

        self::assertCount(1, $loaded);
        self::assertArrayHasKey('simple_20190611', $loaded);
        self::assertInstanceOf(Simple::class, $loaded['simple_20190611']);
    }

    public function testLoadNonExistentDirectoryThrowsException(): void
    {
        $this->expectException(InvalidArgumentException::class);

        new FileSystem(
            __DIR__ . '/../../../Stubs/DoesNotExist',
            'iThemes\\Lib\\Migrate\\Tests\\Stubs\\Migrations'
        );
    }

    public function testLoadEmptyMigrationsDirectoryReturnsEmptyList(): void
    {
        $loader = new FileSystem(
            realpath(__DIR__ . '/../../../Stubs/MigrationsEmpty'),
            'iThemes\\Lib\\Migrate\\Tests\\Stubs\\Migrations'
        );

        self::assertCount(0, $loader->load());
    }

    public function testThrowsExceptionIfClassNotFoundForFile(): void
    {
        // Intentionally broken namespace so as not to trigger composer autoloader.
        $loader = new FileSystem(
            realpath(__DIR__ . '/../../../Stubs/MigrationsClassMismatch'),
            'iThemes\\Lib\\MigrateBroken\\Tests\\Stubs\\MigrationsClassMismatch'
        );
        $this->expectException(ClassNotFound::class);
        $loader->load();
    }

    public function testThrowsExceptionIfClassDoesNotImplementMigrationInterface(): void
    {
        $loader = new FileSystem(
            realpath(__DIR__ . '/../../../Stubs/MigrationsMissingInterface'),
            'iThemes\\Lib\\Migrate\\Tests\\Stubs\\MigrationsMissingInterface'
        );
        $this->expectException(InvalidClass::class);
        $loader->load();
    }

    public function testContainerIntegrationUsesContainerIfHasEntry(): void
    {
        $mock = $this->getMockBuilder(ContainerInterface::class)
            ->setMethods([ 'has', 'get' ])
            ->getMock();

        $mock->expects($this->once())
            ->method('has')
            ->with(Simple::class)
            ->willReturn(true);

        $mock->expects($this->once())
            ->method('get')
            ->with(Simple::class)
            ->willReturn(new Simple());

        $loader = new FileSystem(
            realpath(__DIR__ . '/../../../Stubs/Migrations'),
            'iThemes\\Lib\\Migrate\\Tests\\Stubs\\Migrations'
        );
        $loader->setContainer($mock);

        $loaded = $loader->load();

        self::assertCount(1, $loaded);
        self::assertArrayHasKey('simple_20190611', $loaded);
        self::assertInstanceOf(Simple::class, $loaded['simple_20190611']);
    }

    public function testContainerIntegrationDoesNotUseContainerIfDoesNotHaveEntry(): void
    {
        $mock = $this->getMockBuilder(ContainerInterface::class)
            ->setMethods([ 'has', 'get' ])
            ->getMock();

        $mock->expects($this->once())
            ->method('has')
            ->with(Simple::class)
            ->willReturn(false);

        $mock->expects($this->never())->method('get');

        $loader = new FileSystem(
            realpath(__DIR__ . '/../../../Stubs/Migrations'),
            'iThemes\\Lib\\Migrate\\Tests\\Stubs\\Migrations'
        );
        $loader->setContainer($mock);

        $loaded = $loader->load();

        self::assertCount(1, $loaded);
        self::assertArrayHasKey('simple_20190611', $loaded);
        self::assertInstanceOf(Simple::class, $loaded['simple_20190611']);
    }
}
