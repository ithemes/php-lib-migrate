<?php
declare(strict_types=1);

namespace iThemes\Lib\Migrate\Tests\Suites\Unit\ResultTest;

use iThemes\Lib\Migrate\Exception\ResultSealed;
use iThemes\Lib\Migrate\Migration;
use iThemes\Lib\Migrate\Result;
use iThemes\Lib\Migrate\Tests\Stubs\Migrations\Simple;
use PHPUnit\Framework\TestCase;

class ResultTest extends TestCase
{
    /** @var Migration */
    private static $migration;

    public static function setUpBeforeClass(): void
    {
        static::$migration = new Simple();
    }

    public function testAddSuccess(): void
    {
        $result = new Result();
        $result->addSuccess(static::$migration);

        self::assertTrue($result->isSuccess(static::$migration));
        self::assertFalse($result->isSkipped(static::$migration));
        self::assertFalse($result->isError(static::$migration));

        self::assertCount(1, $result->getAttempted());
        self::assertSame(static::$migration, $result->getAttempted()[0]);
        self::assertFalse($result->hasErrors());
    }

    public function testAddSuccessThrowsIfSealed(): void
    {
        $result = new Result();
        $result->seal();

        $this->expectException(ResultSealed::class);
        $result->addSuccess(static::$migration);
    }

    public function testAddError(): void
    {
        $e = new \Exception();

        $result = new Result();
        $result->addError(static::$migration, $e);

        self::assertFalse($result->isSuccess(static::$migration));
        self::assertFalse($result->isSkipped(static::$migration));
        self::assertTrue($result->isError(static::$migration));
        self::assertSame($e, $result->getError(static::$migration));

        self::assertCount(1, $result->getAttempted());
        self::assertSame(static::$migration, $result->getAttempted()[0]);
        self::assertTrue($result->hasErrors());
    }

    public function testAddErrorThrowsIfSealed(): void
    {
        $result = new Result();
        $result->seal();

        $this->expectException(ResultSealed::class);
        $result->addError(static::$migration, new \Exception());
    }

    public function testAddSkipped(): void
    {
        $result = new Result();
        $result->addSkipped(static::$migration);

        self::assertFalse($result->isSuccess(static::$migration));
        self::assertTrue($result->isSkipped(static::$migration));
        self::assertFalse($result->isError(static::$migration));

        self::assertCount(1, $result->getAttempted());
        self::assertSame(static::$migration, $result->getAttempted()[0]);
        self::assertFalse($result->hasErrors());
    }

    public function testAddSkippedThrowsIfSealed(): void
    {
        $result = new Result();
        $result->seal();

        $this->expectException(ResultSealed::class);
        $result->addSkipped(static::$migration);
    }
}
