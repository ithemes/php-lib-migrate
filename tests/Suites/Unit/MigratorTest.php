<?php
declare(strict_types=1);

namespace iThemes\Lib\Migrate\Tests\Suites\Unit;

use function iThemes\Lib\Migrate\listPluck;
use iThemes\Lib\Migrate\Loader\Instance;
use iThemes\Lib\Migrate\Migration;
use iThemes\Lib\Migrate\Migrator;
use iThemes\Lib\Migrate\Records\InMemory;
use iThemes\Lib\Migrate\Records\Repository;
use PHPUnit\Framework\Constraint\IsIdentical;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class MigratorTest extends TestCase
{
    /** @var int */
    private $lastVersion;

    /** @var Migration[] */
    private $completedUp = [];

    /** @var Migration[] */
    private $completedDown = [];

    public function testUpFollowsVersionOrder(): void
    {
        $m1 = $this->getMigration();
        $m2 = $this->getMigration();

        $this->trackCompletes($m1, $m2);

        $SUT = new Migrator(new Instance($m2, $m1), new InMemory(), new \stdClass());
        $SUT->up();

        self::assertCompletions([ $m1, $m2 ], $this->completedUp);
    }

    public function testUpSkipsCompleted(): void
    {
        $m1 = $this->getMigration();
        $m2 = $this->getMigration();
        $m3 = $this->getMigration();

        $this->trackCompletes($m1, $m2, $m3);

        $repo = new InMemory();
        $repo->record($m1);

        $SUT = new Migrator(new Instance($m1, $m2, $m3), $repo, new \stdClass());
        $SUT->up();

        self::assertCompletions([ $m2, $m3 ], $this->completedUp);
    }

    public function testUpStopsAtVersion(): void
    {
        $m1 = $this->getMigration();
        $m2 = $this->getMigration();

        $this->trackCompletes($m1, $m2);

        $SUT = new Migrator(new Instance($m2, $m1), new InMemory(), new \stdClass());
        $SUT->up(1);

        self::assertCompletions([ $m1 ], $this->completedUp);
    }

    public function testUpSkipsCompletedAndStopsAtVersion(): void
    {
        $m1 = $this->getMigration();
        $m2 = $this->getMigration();
        $m3 = $this->getMigration();

        $this->trackCompletes($m1, $m2, $m3);

        $repo = new InMemory();
        $repo->record($m1);

        $SUT = new Migrator(new Instance($m1, $m2, $m3), $repo, new \stdClass());
        $SUT->up(2);

        self::assertCompletions([ $m2 ], $this->completedUp);
    }

    public function testDownFollowsVersionOrder(): void
    {
        $m1 = $this->getMigration();
        $m2 = $this->getMigration();

        $repo = new InMemory();
        $repo->record($m1, $m2);

        $this->trackCompletes($m1, $m2);

        $SUT = new Migrator(new Instance($m1, $m2), $repo, new \stdClass());
        $SUT->down(1);

        self::assertCompletions([ $m2, $m1 ], $this->completedDown);
    }

    public function testDownStopsAtVersion(): void
    {
        $m1 = $this->getMigration();
        $m2 = $this->getMigration();
        $m3 = $this->getMigration();

        $repo = new InMemory();
        $repo->record($m1, $m2, $m3);

        $this->trackCompletes($m1, $m2, $m3);

        $SUT = new Migrator(new Instance($m1, $m2, $m3), $repo, new \stdClass());
        $SUT->down(2);

        self::assertCompletions([ $m3, $m2 ], $this->completedDown);
    }

    public function testDownSkipsNotCompleted(): void
    {
        $m1 = $this->getMigration();
        $m2 = $this->getMigration();
        $m3 = $this->getMigration();

        $repo = new InMemory();
        $repo->record($m1, $m2);

        $this->trackCompletes($m1, $m2, $m3);

        $SUT = new Migrator(new Instance($m1, $m2, $m3), $repo, new \stdClass());
        $SUT->down(1);

        self::assertCompletions([ $m2, $m1 ], $this->completedDown);
    }

    public function testResetFollowsVersionOrder(): void
    {
        $m1 = $this->getMigration();
        $m2 = $this->getMigration();

        $repo = new InMemory();
        $repo->record($m1, $m2);

        $this->trackCompletes($m1, $m2);

        $SUT = new Migrator(new Instance($m1, $m2), $repo, new \stdClass());
        $SUT->reset();

        self::assertCompletions([ $m2, $m1 ], $this->completedDown);
    }

    public function testResetSkipsNotCompleted(): void
    {
        $m1 = $this->getMigration();
        $m2 = $this->getMigration();
        $m3 = $this->getMigration();

        $repo = new InMemory();
        $repo->record($m1, $m2);

        $this->trackCompletes($m1, $m2, $m3);

        $SUT = new Migrator(new Instance($m1, $m2, $m3), $repo, new \stdClass());
        $SUT->reset();

        self::assertCompletions([ $m2, $m1 ], $this->completedDown);
    }

    public function testResetRunsNotCompletedIfHard(): void
    {
        $m1 = $this->getMigration();
        $m2 = $this->getMigration();
        $m3 = $this->getMigration();

        $repo = new InMemory();
        $repo->record($m1, $m2);

        $this->trackCompletes($m1, $m2, $m3);

        $SUT = new Migrator(new Instance($m1, $m2, $m3), $repo, new \stdClass());
        $SUT->reset(true);

        self::assertCompletions([ $m3, $m2, $m1 ], $this->completedDown);
    }

    public function testUpPassesContext(): void
    {
        $context = new \stdClass();

        $migration = $this->getMigration();
        $migration->expects(self::once())->method('up')->with(new IsIdentical($context));

        $SUT = new Migrator(new Instance($migration), new InMemory(), $context);
        $SUT->up();
    }

    public function testDownPassesContext(): void
    {
        $context = new \stdClass();

        $migration = $this->getMigration();
        $migration->expects(self::once())->method('down')->with(new IsIdentical($context));

        $repo = new InMemory();
        $repo->record($migration);

        $SUT = new Migrator(new Instance($migration), $repo, $context);
        $SUT->down(1);
    }

    public function testResetPassesContext(): void
    {
        $context = new \stdClass();

        $migration = $this->getMigration();
        $migration->expects(self::once())->method('down')->with(new IsIdentical($context));

        $repo = new InMemory();
        $repo->record($migration);

        $SUT = new Migrator(new Instance($migration), $repo, $context);
        $SUT->reset();
    }

    public function testResetHardPassesContext(): void
    {
        $context = new \stdClass();

        $migration = $this->getMigration();
        $migration->expects(self::once())->method('down')->with(new IsIdentical($context));

        $SUT = new Migrator(new Instance($migration), new InMemory(), $context);
        $SUT->reset(true);
    }

    public function testUpMigrationMarkedAsSuccess(): void
    {
        $migration = $this->getMigration();

        $SUT    = new Migrator(new Instance($migration), new InMemory(), new \stdClass());
        $result = $SUT->up();

        self::assertTrue($result->isSuccess($migration));
    }

    public function testUpMigrationMarkedAsErrorIfThrowsException(): void
    {
        $e = new \Exception();

        $migration = $this->getMigration();
        $migration->method('up')->willThrowException($e);

        $SUT    = new Migrator(new Instance($migration), new InMemory(), new \stdClass());
        $result = $SUT->up();

        self::assertTrue($result->isError($migration));
        self::assertSame($e, $result->getError($migration));
    }

    public function testUpMigrationsSubsequentToErrorMarkedAsSkipped(): void
    {
        $m1 = $this->getMigration();
        $m1->method('up')->willThrowException(new \Exception());

        $m2 = $this->getMigration();

        $SUT    = new Migrator(new Instance($m1, $m2), new InMemory(), new \stdClass());
        $result = $SUT->up();

        self::assertTrue($result->isSkipped($m2));
    }

    public function testUpMultipleMigrationStatuses(): void
    {
        $m1 = $this->getMigration();
        $m2 = $this->getMigration();
        $m3 = $this->getMigration();
        $m3->method('up')->willThrowException(new \Exception());
        $m4 = $this->getMigration();

        $SUT    = new Migrator(new Instance($m1, $m2, $m3, $m4), new InMemory(), new \stdClass());
        $result = $SUT->up();

        self::assertTrue($result->isSuccess($m1));
        self::assertTrue($result->isSuccess($m2));
        self::assertTrue($result->isError($m3));
        self::assertTrue($result->isSkipped($m4));
    }

    public function testDownMigrationMarkedAsSuccess(): void
    {
        $migration = $this->getMigration();
        $repo      = new InMemory();
        $repo->record($migration);

        $SUT    = new Migrator(new Instance($migration), $repo, new \stdClass());
        $result = $SUT->down(1);

        self::assertTrue($result->isSuccess($migration));
    }

    public function testDownMigrationMarkedAsErrorIfThrowsException(): void
    {
        $e = new \Exception();

        $migration = $this->getMigration();
        $migration->method('down')->willThrowException($e);

        $repo = new InMemory();
        $repo->record($migration);

        $SUT    = new Migrator(new Instance($migration), $repo, new \stdClass());
        $result = $SUT->down(1);

        self::assertTrue($result->isError($migration));
        self::assertSame($e, $result->getError($migration));
    }

    public function testDownMigrationsSubsequentToErrorMarkedAsSkipped(): void
    {
        $m1 = $this->getMigration();

        $m2 = $this->getMigration();
        $m2->method('down')->willThrowException(new \Exception());

        $repo = new InMemory();
        $repo->record($m1, $m2);

        $SUT    = new Migrator(new Instance($m1, $m2), $repo, new \stdClass());
        $result = $SUT->down(1);

        self::assertTrue($result->isSkipped($m1));
    }

    public function testDownMultipleMigrationStatuses(): void
    {
        $m1 = $this->getMigration();
        $m2 = $this->getMigration();
        $m2->method('down')->willThrowException(new \Exception());
        $m3 = $this->getMigration();
        $m4 = $this->getMigration();

        $repo = new InMemory();
        $repo->record($m1, $m2, $m3, $m4);

        $SUT    = new Migrator(new Instance($m1, $m2, $m3, $m4), $repo, new \stdClass());
        $result = $SUT->down(1);

        self::assertTrue($result->isSuccess($m4));
        self::assertTrue($result->isSuccess($m3));
        self::assertTrue($result->isError($m2));
        self::assertTrue($result->isSkipped($m1));
    }

    public function testSuccessfulUpsAreRecorded(): void
    {
        /** @var Repository $repo */
        $repo      = new InMemory();
        $migration = $this->getMigration();

        $SUT = new Migrator(new Instance($migration), $repo, new \stdClass());
        $SUT->up();

        self::assertCount(1, $repo->getCompleted());
        self::assertEquals($migration->getId(), $repo->getCompleted()[0]->getId());
    }

    public function testErrorUpsAreNotRecorded(): void
    {
        /** @var Repository $repo */
        $repo      = new InMemory();
        $migration = $this->getMigration();
        $migration->method('up')->willThrowException(new \Exception());

        $SUT = new Migrator(new Instance($migration), $repo, new \stdClass());
        $SUT->up();

        self::assertCount(0, $repo->getCompleted());
    }

    public function testSkippedUpsAreNotRecorded(): void
    {
        /** @var Repository $repo */
        $repo = new InMemory();
        $m1   = $this->getMigration();
        $m1->method('up')->willThrowException(new \Exception());

        $m2 = $this->getMigration();

        $SUT = new Migrator(new Instance($m1, $m2), $repo, new \stdClass());
        $SUT->up();

        self::assertCount(0, $repo->getCompleted());
    }

    public function testUpMultipleRecord(): void
    {
        /** @var Repository $repo */
        $repo = new InMemory();

        $m1 = $this->getMigration();
        $m2 = $this->getMigration();
        $m3 = $this->getMigration();
        $m3->method('up')->willThrowException(new \Exception());
        $m4 = $this->getMigration();

        $SUT = new Migrator(new Instance($m1, $m2, $m3, $m4), $repo, new \stdClass());
        $SUT->up();

        self::assertCount(2, $repo->getCompleted());
        self::assertEquals($m1->getId(), $repo->getCompleted()[0]->getId());
        self::assertEquals($m2->getId(), $repo->getCompleted()[1]->getId());
    }

    public function testSuccessfulDownsAreDeleted(): void
    {
        $repo      = new InMemory();
        $migration = $this->getMigration();

        $repo->record($migration);

        $SUT = new Migrator(new Instance($migration), $repo, new \stdClass());
        $SUT->down(1);

        self::assertCount(0, $repo->getCompleted());
    }

    public function testErrorDownsAreNotRecorded(): void
    {
        $repo      = new InMemory();
        $migration = $this->getMigration();
        $migration->method('down')->willThrowException(new \Exception());

        $repo->record($migration);

        $SUT = new Migrator(new Instance($migration), $repo, new \stdClass());
        $SUT->down(1);

        self::assertCount(1, $repo->getCompleted());
    }

    public function testSkippedDownsAreNotRecorded(): void
    {
        $repo = new InMemory();

        $m1 = $this->getMigration();
        $m2 = $this->getMigration();
        $m2->method('down')->willThrowException(new \Exception());

        $repo->record($m1, $m2);

        $SUT = new Migrator(new Instance($m1, $m2), $repo, new \stdClass());
        $SUT->down(1);

        self::assertCount(2, $repo->getCompleted());
    }

    public function testDownMultipleRecord(): void
    {
        /** @var Repository $repo */
        $repo = new InMemory();

        $m1 = $this->getMigration();
        $m2 = $this->getMigration();
        $m2->method('down')->willThrowException(new \Exception());
        $m3 = $this->getMigration();
        $m4 = $this->getMigration();

        $repo->record($m1, $m2, $m3, $m4);

        $SUT = new Migrator(new Instance($m1, $m2, $m3, $m4), $repo, new \stdClass());
        $SUT->down(1);

        self::assertCount(2, $repo->getCompleted());
        self::assertEquals($m1->getId(), $repo->getCompleted()[0]->getId());
        self::assertEquals($m2->getId(), $repo->getCompleted()[1]->getId());
    }

    /**
     * Assert that the given migrations were run and in the expected order.
     *
     * @param Migration[] $expected
     * @param Migration[] $actual
     */
    private static function assertCompletions(array $expected, array $actual): void
    {
        self::assertSame(listPluck($expected, 'getId'), listPluck($actual, 'getId'));
    }

    private function trackCompletes(MockObject ...$mocks): void
    {
        foreach ($mocks as $mock) {
            $mock->method('up')->willReturnCallback(function () use ($mock) {
                $this->completedUp[] = $mock;
            });
            $mock->method('down')->willReturnCallback(function () use ($mock) {
                $this->completedDown[] = $mock;
            });
        }
    }

    /**
     * Get a migration mock.
     *
     * @param int    $version
     * @param string $id
     *
     * @return Migration|MockObject
     */
    private function getMigration(?int $version = null, ?string $id = null)
    {
        $mock = $this->getMockBuilder(Migration::class)
            ->setMethods([ 'getId', 'getVersion', 'up', 'down', '__toString' ])
            ->getMock();

        $version = $version ?? ++$this->lastVersion;
        $id      = $id ?? ($version . get_class($mock));
        $mock->method('getId')->willReturn($id);
        $mock->method('getVersion')->willReturn($version);
        $mock->method('__toString')->willReturn($id);

        return $mock;
    }
}
